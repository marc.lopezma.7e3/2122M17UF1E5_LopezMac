﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    private Vector2 mousePosition;

    private float aimAngle;
    private Vector2 aimDirection;
    public Transform firePoint;

    public Rigidbody2D rb;
    public Camera sceneCamera;

    [SerializeField]
    public GameObject shootEffect;
    [SerializeField]
    public GameObject bullet;
    [SerializeField]
    public float fireForce;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        sceneCamera = Camera.main;
    }

    void FixedUpdate()
    {
        UpdateMousePosition();
        UpdateShootingAngle();
        LetItRain();
    }

    private void UpdateMousePosition()
    {
        mousePosition = sceneCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    private void UpdateShootingAngle()
    {
        aimDirection = mousePosition - rb.position;
        aimAngle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;
        gameObject.transform.rotation = Quaternion.Euler(new Vector3(0,0,aimAngle));
    }

    private void LetItRain()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        // Instantiate(shootEffect, firePoint.position, firePoint.rotation);

        GameObject projectile = Instantiate(bullet, firePoint.position, Quaternion.identity);
        projectile.GetComponent<Rigidbody2D>().AddForce(aimDirection * fireForce, ForceMode2D.Impulse);

    }

    private void IncreaseScore()
    {
        GameManager.Instance.IncreaseScore(1);
    }

}