﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    void Start() 
    {

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        switch (collider.gameObject.tag)
        {
            case "Enemy":
                collider.GetComponent<Enemy>().Die();
                break;
            case "Floor":
                // Impact();
                Destroy(gameObject);
                break;
            case "Player":
                Debug.Log("Hit player");
                // collider.GetComponent<Player>().Damage(bulletDamage);
                // Impact();
                break;
        }
    }

}
